import time
import serial
import serial.tools.list_ports
import asyncio
import statistics
import requests
import threading

from WaterRowerConnection import WaterRowerConnection


# Protocol:
# Hard pull is 1, Soft pull is 0

# R Ls Ns Le Ne D
# R: 1 bit for showing you are ready (Boolean, 1 and 0 are both continue)
# 12 bits for a chess move in Long algebraic notation (e2e4)
# Ls: Letter start position (3 bits, 000 is a, 111 is h)
# Ns: Number start position  (3 bits, 000 is 1, 111 is 8)
# Le: Letter end position (3 bits, 000 is a, 111 is h)
# Ne: Number end position  (3 bits, 000 is 1, 111 is 8)
# D: Done (Boolean 1 is done, 0 is retry)
# A new input is excepted if the pulses go down a certain threshold.
# Then when the protocol reaches a certain threshold again, it reads until
# the max pulses is read and then checks if it is 1 or 0

AVERAGE_AMOUNT = 10
HARD_PULL_PULSES = 10
LOW_PULL_THRESHOLD = 4
SLEEP_TIME = 0.01
PLAYER = 1
SERVER = "insertserverhere.com"


def send_post_request(url, data):
    requests.post(url, data=data)


async def send_post_request_server(data):
    headers = {'Content-Type': 'application/json'}  # Example headers, modify as needed
    requests.post(SERVER, json=data, headers=headers)


class DJChessCode:
    def __init__(self):
        self.port = None
        self.connection = None
        self.pulses = 0
        self.pulses_readouts = []
        self.avg_pulse = 0
        self.running_tasks = set()

    def on_disconnect(self):
        self.connection = None

    async def start_reader(self):
        task = asyncio.create_task(self.run())
        self.running_tasks.add(task)
        task.add_done_callback(self.running_tasks.remove)

    async def read_bit(self) -> int:
        max_pulses = 0
        if self.connection:
            # Wait for pulses to get lower than a certain threshold
            while self.pulses >= 7.5:
                await asyncio.sleep(SLEEP_TIME)
            print("go")

            # # Read until the pulses go down and it goes over a certain threshold
            while self.pulses <= 8.5:
                await asyncio.sleep(SLEEP_TIME)

            max_pulses = self.pulses
            while True:
                await asyncio.sleep(SLEEP_TIME)
                if max_pulses <= self.pulses:
                    max_pulses = self.pulses
                else:
                    break

            if max_pulses > 11:
                print("1")
                return 1
        print("0")
        return 0
    
    async def read_bit_avg(self) -> int:
        if self.connection:
            # Wait for pulses to get lower than a certain threshold
            while self.avg_pulse > 7.5:
                await asyncio.sleep(SLEEP_TIME)
            
            print("go")
            data = {
                'player': PLAYER,
                'bit': 'go'
            }
            await send_post_request_server(data)

            # Read until the pulses go again over a certain threshold
            while self.avg_pulse < 8.0:
                await asyncio.sleep(SLEEP_TIME)

            max_pulses = self.avg_pulse
            while True:
                await asyncio.sleep(SLEEP_TIME)
                if max_pulses <= self.avg_pulse:
                    max_pulses = self.avg_pulse
                else:
                    break

            if max_pulses > 11.5:
                print("1")
                data = {
                    'player': PLAYER,
                    'bit': 1
                }
                await send_post_request_server(data)

                return 1
        print("0")
        data = {
            'player': PLAYER,
            'bit': 0
        }
        # thread = threading.Thread(target=send_post_request, args=(SERVER, data))
        # thread.start()
        await send_post_request_server(data)

        return 0

    async def read_value(self) -> int:
        value = 0
        for i in range(2, -1, -1):
            # value += await self.read_bit() << i
            value += await self.read_bit_avg() << i
        return value + 1
        
    async def get_move(self):
        # Check if the user is ready?
        print("Ready?")
        # _ = await self.read_bit()
        _ = await self.read_bit_avg()
        while True:
            move = ""
            print("Fill in move")
            for i in range(4):
                if i % 2:
                    move += str(await self.read_value())
                else:
                    move += self.convert_to_letter(await self.read_value())
            print(f"Do you accept move: {move} (pull hard for yes, soft for no)?")
            # if await self.read_bit():
            if await self.read_bit_avg():
                self.is_ready_for_input = False
                return move

    @staticmethod
    def convert_to_letter(value):
        return chr(value + 96)

    async def run(self):
        try:
            # Start monitoring in the background, calling onEvent when data comes in.
            self.connect()

            # This is where you run your main application. For instance, you could start a Flask app here,
            # run a GUI, do a full-screen blessed virtualization, or just about anything else.
            while self.connection:
                await asyncio.sleep(10)
        finally:
            print("Closing")
            if self.connection:
                self.connection.close()

    def on_event(self, event):
        """Called when any data comes."""
        if event["type"] == "pulse":
            self.pulses = event["value"]

            # Calculate average to stop the bouncing effect
            self.pulses_readouts.append(event["value"])
            if len(self.pulses_readouts) > AVERAGE_AMOUNT:
                self.avg_pulse = statistics.mean(self.pulses_readouts)
                self.pulses_readouts.pop(0)
            
            # print(self.avg_pulse)
            
    def connect(self):
        """This will start a thread in the background, that will call the onEvent method whenever data comes in."""
        self.port = self.find_port()
        print("Connecting to WaterRower on port %s" % self.port)
        self.connection = WaterRowerConnection(self.port, self.on_disconnect, self.on_event)
        self.connection.start()

    @staticmethod
    def find_port():
        attempts = 0
        while True:
            attempts += 1
            ports = serial.tools.list_ports.comports()
            for path, name, _ in ports:
                if "WR" in name:
                    return path

            # message every ~10 seconds
            if attempts % 10 == 0:
                print("Port not found in %d attempts; retrying every 5s" % attempts)

            time.sleep(1)


async def main():
    # running_tasks = set()
    # task = asyncio.create_task(dj_chess_code.run())
    # running_tasks.add(task)
    # task.add_done_callback(running_tasks.remove)
    dj_chess_code = DJChessCode()
    await dj_chess_code.start_reader()

    while True:
        print(await asyncio.create_task(dj_chess_code.get_move()))


if __name__ == "__main__":
    asyncio.run(main())
