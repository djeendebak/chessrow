ChessRow/DJ Chess Code - A Chess Rowing Game

A game build in about 6 hours with 2 people for the third End of Quarter 2023/2024 schoolyear of the Associate Degree Software Development from Saxion.

# Requirements
- 2 Rowing machines
- 2 Computers

# Install and run
- Both: Connect to rowing machine
- Both: Poetry install
- Both: Change the Server and Player variable in chesscode.py (Player 1 or 2, 1 for white 2 for black)
- Both: Run the chesscode.py (This will send 0 and 1 to the server based on how hard the player pulls) 
- Main PC: Poetry run Python app.py (This is the server, currently set to port 3000, so make sure this port is exposed and reachable through some address)
- Main PC: Display (using a live server so it updates when the file gets changed) the chessboard.html this shows the board, codes for moves and when ready to go

# Known Bugs
- Go button not 100% accurate (specifically when start turn or when player pulling before go is green)

# Features not yet implemented
- 1 Player chess vs Bot
- Finetuning the hard/soft pull for different players (some people naturally pull harder, maybe solvable through a setup phase)
- End game (Functions are build in to check for end/draw but not yet implemented)

