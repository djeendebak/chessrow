import chess
import chess.svg
from flask import Flask, render_template, request
from bs4 import BeautifulSoup, NavigableString

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True


class Game:
    def __init__(self):
        self.board = chess.Board()
        self.player_turn = 1
        self.current_input = ""
        self.print_board()

    def check_end(self):
        if self.board.is_stalemate() or self.board.is_insufficient_material():
            print("Game ended in draw")
            return True
        
        if self.board.is_checkmate():
            print("Game is checkmate")
            return True
        return False

    def play_turn(self, new_input):
        try:
            move = chess.Move.from_uci(new_input)
        except chess.InvalidMoveError:
            return
        if move not in self.board.legal_moves:
            pass
        else:
            self.board.push(move)
            if self.player_turn == 1:
                self.player_turn = 2
            else:
                self.player_turn = 1
            self.print_board()
            return

    def print_board(self):
        svg = chess.svg.board(board=self.board)
        if self.player_turn == 1:
            svg = svg.replace("#212121", "#FFFFFF")
            svg = svg.replace("#e5e5e5", "#212121")
        with open("board.svg", "w") as f:
            f.write(svg)


def change_input(new_input):
    with open("chessboard.html", "r") as f:
        html_content = f.read()
        soup = BeautifulSoup(html_content, 'html.parser')
        div_elements = soup.find_all('div', class_='input')
        for element in div_elements:
            if type(element) == NavigableString:
                pass
            else:
                element.string = new_input
    # Write the modified content back to the file
    with open("chessboard.html", "w") as f:
        f.write(str(soup))


def change_go(green):
    with open("chessboard.html", "r") as f:
        html_content = f.read()
        soup = BeautifulSoup(html_content, 'html.parser')
        div_elements = soup.find_all('img', class_='go')
        for element in div_elements:
            if type(element) == NavigableString:
                pass
            else:
                if green:
                    element['src'] = "icongo-green.png"
                else:
                    element['src'] = "icongo-red.png"
    # Write the modified content back to the file
    with open("chessboard.html", "w") as f:
        f.write(str(soup))


def convert_input(value):
    binary_to_combo_dict = {
        '000': 'a/1', '001': 'b/2', '010': 'c/3', '011': 'd/4',
        '100': 'e/5', '101': 'f/6', '110': 'g/7', '111': 'h/8'
    }
    
    combos = []
    
    for i in range(0, len(value), 3):
        chunk = value[i:i+3]
        combos.extend(binary_to_combo_dict[chunk].split('/'))

    combo_string = "".join(combos)
    return combo_string[0] + combo_string[3] + combo_string[4] + combo_string[7]


game = Game()


@app.route("/", methods=["POST"])
def receive_data():
    print(request)
    data = request.get_json()

    if data["player"] == game.player_turn:
        if data["bit"] == "go":
            change_go(True)
        elif data["bit"] == 1 or data["bit"] == 0:
            game.current_input += str(data["bit"])
            if len(game.current_input) == 12:
                game.current_input = convert_input(game.current_input) + "? confirm with 1"
            elif "?" in game.current_input:
                if data["bit"] == 1:
                    game.play_turn(game.current_input[0:4])
                game.current_input = ""

            change_input(game.current_input)
            change_go(False)
    return "Hello World"


if __name__ == '__main__':
    change_input("")
    app.run(host='0.0.0.0')
